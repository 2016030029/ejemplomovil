package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txtNum1;
    private  EditText txtNum2;
    private EditText txtResult;
    private Button   btnSumar, btnRestar, btnMulti, btnDiv, btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniComponents();
    }

    public void iniComponents(){
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        txtResult = findViewById(R.id.txtResult);

        btnSumar = findViewById(R.id.btnSuma);
        btnRestar = findViewById(R.id.btnResta);
        btnMulti = findViewById(R.id.btnMult);
        btnDiv = findViewById(R.id.btnDivi);

        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);

        setEventos();
    }
    public void setEventos(){
        this.btnCerrar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnSumar.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btnSuma:

                if(txtNum1.getText().toString().equals("") || txtNum2.getText().toString().equals("")) {
                    Toast.makeText(this, "Ingrese todos los valores ", Toast.LENGTH_SHORT).show();

                }else {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResult.setText(String.valueOf(op.suma()));
                }
            break;

            case R.id.btnResta:
                if(txtNum1.getText().toString().equals("") || txtNum2.getText().toString().equals("")) {
                    Toast.makeText(this, "Ingrese todos los valores ", Toast.LENGTH_SHORT).show();

                }else {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResult.setText(String.valueOf(op.resta()));
                }
                break;

            case R.id.btnMult:
                if(txtNum1.getText().toString().equals("") || txtNum2.getText().toString().equals("")) {
                    Toast.makeText(this, "Ingrese todos los valores ", Toast.LENGTH_SHORT).show();

                }else {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResult.setText(String.valueOf(op.mult()));
                }
                break;

            case R.id.btnDivi:
                if(txtNum1.getText().toString().equals("") || txtNum2.getText().toString().equals("")) {
                    Toast.makeText(this, "Ingrese todos los valores ", Toast.LENGTH_SHORT).show();

                }else {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResult.setText(String.valueOf(op.div()));
                }
                break;

            case R.id.btnLimpiar:
                txtNum1.setText("");
                txtNum2.setText("");
                txtResult.setText("");
                break;

            case R.id.btnCerrar:
                System.exit(1);
                break;
        }



        }


    }
